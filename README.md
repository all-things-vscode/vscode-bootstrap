# VSCode Bootstrap

- [Extensions](./EXTENSIONS.md)

- [Terminal](./TERMINAL.md)

- [Fonts](./FONTS.md)

- [License - MIT](./LICENSE)

## Author

>(c) Jan Schulte
>
>Date: 2020-02-10 21:26:47
