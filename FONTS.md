# [VSCode Bootstrap:](./README.md) ***Fonts***

## Custom fonts

### [Fira Code](https://github.com/tonsky/FiraCode)

*`Monospaced font with programming ligatures`*

- [Download](https://github.com/tonsky/FiraCode/releases)

- [Installation](https://github.com/tonsky/FiraCode/wiki/VS-Code-Instructions)

### [JetBrains Mono](https://www.jetbrains.com/lp/mono/)

*`A typeface for developers`*

- [Download](https://download.jetbrains.com/fonts/JetBrainsMono-1.0.2.zip)

- [Installation](https://www.jetbrains.com/lp/mono/#how-to-install)
