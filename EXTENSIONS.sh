#!/bin/bash

echo "Install VSCode Extensions"

code --install-extension CoenraadS.bracket-pair-colorizer-2 --force && \
code --install-extension oderwat.indent-rainbow --force && \
code --install-extension mikestead.dotenv --force && \
code --install-extension TabNine.tabnine-vscode --force && \
code --install-extension christian-kohler.path-intellisense --force && \
code --install-extension alefragnani.project-manager --force && \
code --install-extension Gruntfuggly.todo-tree --force && \
code --install-extension streetsidesoftware.code-spell-checker --force && \
code --install-extension jsynowiec.vscode-insertdatestring --force && \
code --install-extension vscode-icons-team.vscode-icons --force && \
code --install-extension joelday.docthis --force && \
code --install-extension wix.vscode-import-cost --force && \
code --install-extension esbenp.prettier-vscode --force && \
code --install-extension dbaeumer.vscode-eslint --force && \
code --install-extension Orta.vscode-jest --force && \
code --install-extension Stepsize.tech-debt-tracker --force && \
code --install-extension pranaygp.vscode-css-peek --force && \
code --install-extension ms-vscode.csharp --force && \
code --install-extension Fudge.auto-using --force && \
code --install-extension k--kato.docomment --force && \
code --install-extension formulahendry.dotnet-test-explorer --force && \
code --install-extension jmrog.vscode-nuget-package-manager --force && \
code --install-extension donjayamanne.git-extension-pack --force && \
code --install-extension eamodio.gitlens --force && \
code --install-extension KnisterPeter.vscode-commitizen --force && \
code --install-extension ms-azuretools.vscode-docker --force && \
code --install-extension MS-vsliveshare.vsliveshare --force
