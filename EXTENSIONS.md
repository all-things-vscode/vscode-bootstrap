# [VSCode Bootstrap:](./README.md) ***Extensions***

[Bash Installation Script](./EXTENSIONS.sh)

```bash
# Install all below listed extensions via bash shell

$ sh EXTENSIONS.sh
```

## Editor

### Highlighting

* [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2) - [Install](vscode:extension/CoenraadS.bracket-pair-colorizer-2)

* [Indent Rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow) - [Install](vscode:extension/oderwat.indent-rainbow)

* [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) - [Install](vscode:extension/mikestead.dotenv)

### Intellisense / Auto-Completer

* [TabNine](https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode) - [Install](vscode:extension/TabNine.tabnine-vscode)

* [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) - [Install](vscode:extension/christian-kohler.path-intellisense)

### Organizing

* [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager) - [Install](vscode:extension/alefragnani.project-manager)

* [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree) - [Install](vscode:extension/Gruntfuggly.todo-tree)

### Utilities

* [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) - [Install](vscode:extension/streetsidesoftware.code-spell-checker)

* [Insert Date String](https://marketplace.visualstudio.com/items?itemName=jsynowiec.vscode-insertdatestring) - [Install](vscode:extension/jsynowiec.vscode-insertdatestring)

### Icons

* [VSCode Icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons) - [Install](vscode:extension/vscode-icons-team.vscode-icons)

### Languages / Frameworks / Tools

#### JavaScript / TypeScript

* [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis) - [Install](vscode:extension/joelday.docthis)

* [Import Cost](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost) - [Install](vscode:extension/wix.vscode-import-cost)

* [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - [Install](vscode:extension/esbenp.prettier-vscode)

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - [Install](vscode:extension/dbaeumer.vscode-eslint)

* [Jest](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest) - [Install](vscode:extension/Orta.vscode-jest)

* [Tech Debt Tracker](https://marketplace.visualstudio.com/items?itemName=Stepsize.tech-debt-tracker) - [Install](vscode:extension/Stepsize.tech-debt-tracker)

#### CSS

* [css-peek](https://marketplace.visualstudio.com/items?itemName=pranaygp.vscode-css-peek) - [Install](vscode:extension/pranaygp.vscode-css-peek)

#### C\#

* [C#](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp) - [Install](vscode:extension/ms-vscode.csharp)

* [Auto-Using for C#](https://marketplace.visualstudio.com/items?itemName=Fudge.auto-using) - [Install](vscode:extension/Fudge.auto-using)

* [C# XML Documentation Comments](https://marketplace.visualstudio.com/items?itemName=k--kato.docomment) - [Install](vscode:extension/k--kato.docomment)

* [.NET Core Test Explorer](https://marketplace.visualstudio.com/items?itemName=formulahendry.dotnet-test-explorer) - [Install](vscode:extension/formulahendry.dotnet-test-explorer)

* [NuGet Package Manager](https://marketplace.visualstudio.com/items?itemName=jmrog.vscode-nuget-package-manager) - [Install](vscode:extension/jmrog.vscode-nuget-package-manager)

#### GIT

* [Git Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.git-extension-pack) - [Install](vscode:extension/donjayamanne.git-extension-pack)

* [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - [Install](vscode:extension/eamodio.gitlens)

* [Git Commitizen](https://marketplace.visualstudio.com/items?itemName=KnisterPeter.vscode-commitizen) - [Install](vscode:extension/KnisterPeter.vscode-commitizen)

#### Docker

* [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) - [Install](vscode:extension/ms-azuretools.vscode-docker)

#### Team

* [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare) - [Install](vscode:extension/MS-vsliveshare.vsliveshare)
