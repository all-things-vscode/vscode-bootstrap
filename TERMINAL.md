# [VSCode Bootstrap:](./README.md) ***Terminal***

## Custom terminal

### [Cmder](http://cmder.net)

*`Lovely console emulator package for Windows`*

- [Download](https://github.com/cmderdev/cmder/releases/)

- [Installation](https://github.com/cmderdev/cmder/wiki/Seamless-VS-Code-Integration)
